//Addition
function sumOf(x,y){
	let sum = x + y;
	console.log("Displayed sum of " + x + " and " + y);
	console.log(sum)
}
sumOf(5,15);

// Subtraction
function differenceOf(x,y){
	let difference = x - y;
	console.log("Displayed difference of " + x + " and " + y);
	console.log(difference);
}
differenceOf(20, 5);

// Multiplication
function productOf(x,y){
	console.log("The product of " + x + " and " + y + ":");
	return x*y;
}
let product = productOf(50,10);
console.log(product);

// Division
function quotientOf(x,y){
	console.log("The quotient of " + x + " and " + y + ":")
	return x/y;
}
let quotient = quotientOf(50,10);
console.log(quotient);

// Area of circle
function areaOfCircleWithGivenRadius(num){
	console.log("The result of getting the are of a circle with " + num + " radius:");
	return 3.1416*(num**2);
}
let circleArea = areaOfCircleWithGivenRadius(15);
console.log(circleArea);

// Average of 4 numbers
function averageOfFour(num1,num2,num3,num4){
	let total = num1 + num2 + num3 + num4;
	let average = total / 4;
	console.log("The average of " + num1 + ", " + num2 + ", " + num3 + ", " + num4 + ":");
	return average;
}

let averageVar = averageOfFour(20,40,60,80);
console.log(averageVar);

// Score
function passCalc(x,y){
	let percent = (x/y)*100;
	let isPassed = percent >= 75;
	console.log("Is " + x + "/" + y + " passing score?");
	return isPassed;
}

let isPassingScore = passCalc(38,50);
console.log(isPassingScore);